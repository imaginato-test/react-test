import React, { Component } from "react";
import { connect } from "react-redux";
import DashboardView from "./DashboardView";
import * as dashboardActions from "../../actions";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "./style.css";

export class DashboardContainer extends Component {
  render() {
    return (
      <>
        <DashboardView {...this.props} />
        <ToastContainer />
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.auth,
  dashboard: state.dashboard,
  isLoading: state.dashboard.isLoading,
});

const mapDispatchToProps = (dispatch) => {
  return {
    addDashboardOrder: (data) =>
      dispatch(dashboardActions.addDashboardOrder(data)),
    updateDashboardOrder: (data) =>
      dispatch(dashboardActions.updateDashboardOrder(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DashboardContainer);

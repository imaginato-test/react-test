import React, { Component } from "react";
import SimpleReactValidator from "simple-react-validator";
import moment from "moment";
import { toast } from "react-toastify";

export class DashboardView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      orderName: "",
      orderPrice: "",
      orderNotes: "",
      holdModalEditPrDeleteObj: null,
    };

    this.validator = new SimpleReactValidator();
  }

  handleAddNewOrder = (e) => {
    e.preventDefault();
    if (this.validator.allValid()) {
      let objToAdd = {
        orderName: this.state.orderName,
        orderPrice: this.state.orderPrice,
        orderNotes: this.state.orderNotes,
        addedAt: moment.now(),
      };
      // called add order action
      this.props.addDashboardOrder({
        objToAdd,
      });

      setTimeout(() => {
        this.removeElementsByClass("modal-backdrop");
        document.getElementById("addOrderModal").classList.remove("show");
        this.setState({
          orderName: "",
          orderPrice: "",
          orderNotes: "",
        });
      }, 500);
      toast.success("Order has been created successfully");
    } else {
      // rerender to show messages for the first time
      this.validator.showMessages();
      this.forceUpdate();
    }
  };

  removeElementsByClass(className) {
    var elements = document.getElementsByClassName(className);
    while (elements.length > 0) {
      elements[0].parentNode.removeChild(elements[0]);
    }
  }

  setEditOrderState = (order) => {
    if (order) {
      this.setState({
        orderName: order.orderName,
        orderPrice: order.orderPrice,
        orderNotes: order.orderNotes,
        holdModalEditPrDeleteObj: order,
      });
    }
  };

  handleEditOrder = (e) => {
    e.preventDefault();
    if (
      this.validator.allValid() &&
      this.state.holdModalEditPrDeleteObj !== null
    ) {
      let ordersList = [...this.props.dashboard.orders];
      let findIndex = ordersList.findIndex(
        (item) => item.addedAt === this.state.holdModalEditPrDeleteObj.addedAt
      );
      if (findIndex !== -1) {
        ordersList[findIndex] = {
          ...ordersList[findIndex],
          orderName: this.state.orderName,
          orderPrice: this.state.orderPrice,
          orderNotes: this.state.orderNotes,
        };

        // called update order action
        this.props.updateDashboardOrder(ordersList);

        setTimeout(() => {
          this.removeElementsByClass("modal-backdrop");
          document.getElementById("editOrderModal").classList.remove("show");
          this.setState({
            orderName: "",
            orderPrice: "",
            orderNotes: "",
            holdModalEditPrDeleteObj: null,
          });
          toast.success("Order has been updated successfully");
        }, 500);
      }
    } else {
      // rerender to show messages for the first time
      this.validator.showMessages();
      this.forceUpdate();
    }
  };

  setDeleteOrderState = (order) => {
    if (order) {
      this.setState({
        holdModalEditPrDeleteObj: order,
      });
    }
  };

  handleDeleteConfirm = (e) => {
    e.preventDefault();
    if (this.state.holdModalEditPrDeleteObj !== null) {
      let ordersList = [...this.props.dashboard.orders];
      let findIndex = ordersList.findIndex(
        (item) => item.addedAt === this.state.holdModalEditPrDeleteObj.addedAt
      );
      if (findIndex !== -1) {
        ordersList.splice(findIndex, 1);

        // called update order action
        this.props.updateDashboardOrder(ordersList);

        setTimeout(() => {
          this.removeElementsByClass("modal-backdrop");
          document.getElementById("deleteOrderModal").classList.remove("show");
          this.setState({
            holdModalEditPrDeleteObj: null,
          });
          toast.success("Order has been deleted successfully");
        }, 500);
      }
    }
  };

  render() {
    const { dashboard } = this.props;
    return (
      <div>
        <div className="container-xl">
          <div className="csLine mt-5 mb-5">
            <h2>
              <span>Daily Drinks</span>
            </h2>
          </div>
          <div className="table-responsive">
            <div className="table-wrapper">
              <div className="table-title">
                <div className="row">
                  <div className="col-sm-6">
                    <h2>
                      Manage <b>Orders</b>
                    </h2>
                  </div>
                  <div className="col-sm-6">
                    <a
                      href="#addOrderModal"
                      className="btn btn-success"
                      data-toggle="modal"
                    >
                      <i className="material-icons">&#xE147;</i>{" "}
                      <span>Add New Order</span>
                    </a>
                  </div>
                </div>
              </div>
              <table className="table table-striped table-hover">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Order Name</th>
                    <th>Order Price</th>
                    <th>Order Notes</th>
                    <th>Added At</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {dashboard.orders.map((order, index) => {
                    return (
                      <tr key={index}>
                        <td>{index + 1} </td>
                        <td>{order.orderName}</td>
                        <td>{"$ " + order.orderPrice}</td>
                        <td>
                          {order.orderNotes.substr(0, 60)}
                          {order.orderNotes.length > 60 ? "..." : ""}
                        </td>
                        <td>{moment(order.addedAt).fromNow(true) + " ago"}</td>
                        <td>
                          <a
                            href="#editOrderModal"
                            className="edit"
                            data-toggle="modal"
                            onClick={() => this.setEditOrderState(order)}
                          >
                            <i
                              className="material-icons"
                              data-toggle="tooltip"
                              title="Edit"
                            >
                              &#xE254;
                            </i>
                          </a>
                          <a
                            href="#deleteOrderModal"
                            className="delete"
                            data-toggle="modal"
                            onClick={() => this.setDeleteOrderState(order)}
                          >
                            <i
                              className="material-icons"
                              data-toggle="tooltip"
                              title="Delete"
                            >
                              &#xE872;
                            </i>
                          </a>
                        </td>
                      </tr>
                    );
                  })}

                  {/* if no order found */}
                  {dashboard.orders.length === 0 && (
                    <tr>
                      <td colSpan="7" className="text-center">
                        No orders found
                      </td>
                    </tr>
                  )}
                </tbody>
              </table>
            </div>
          </div>
        </div>

        {/* Add Modal JSX */}
        <div id="addOrderModal" className="modal fade">
          <div className="modal-dialog">
            <div className="modal-content">
              <form method="post" onSubmit={this.handleAddNewOrder}>
                <div className="modal-header">
                  <h4 className="modal-title">Add Order</h4>
                  <button
                    type="button"
                    className="close"
                    data-dismiss="modal"
                    aria-hidden="true"
                  >
                    ×
                  </button>
                </div>
                <div className="modal-body">
                  <div className="form-group">
                    <label>
                      Order Name <span className="text-danger">*</span>
                    </label>
                    <input
                      type="text"
                      name="orderName"
                      className="form-control"
                      value={this.state.orderName}
                      onChange={(e) =>
                        this.setState({ orderName: e.target.value })
                      }
                    />
                    {this.validator.message(
                      "orderName",
                      this.state.orderName,
                      "required|alpha_num_space|max:30",
                      { className: "text-danger" }
                    )}
                  </div>
                  <div className="form-group">
                    <label>
                      Order Price <span className="text-danger">*</span>
                    </label>
                    <input
                      type="text"
                      className="form-control"
                      name="orderPrice"
                      value={this.state.orderPrice}
                      onChange={(e) =>
                        this.setState({ orderPrice: e.target.value })
                      }
                    />
                    {this.validator.message(
                      "orderPrice",
                      this.state.orderPrice,
                      "required|numeric|min:1,num",
                      { className: "text-danger" }
                    )}
                  </div>
                  <div className="form-group">
                    <label>Order Notes</label>
                    <textarea
                      className="form-control"
                      value={this.state.orderNotes}
                      onChange={(e) =>
                        this.setState({ orderNotes: e.target.value })
                      }
                    />
                  </div>
                </div>
                <div className="modal-footer">
                  <input
                    type="button"
                    className="btn btn-default"
                    data-dismiss="modal"
                    defaultValue="Cancel"
                  />
                  <input
                    type="submit"
                    className="btn btn-success"
                    defaultValue="Add"
                  />
                </div>
              </form>
            </div>
          </div>
        </div>
        {/* Add Modal JSX */}

        {/* Edit Modal JSX */}
        <div id="editOrderModal" className="modal fade">
          <div className="modal-dialog">
            <div className="modal-content">
              <form method="post" onSubmit={this.handleEditOrder}>
                <div className="modal-header">
                  <h4 className="modal-title">Edit Order</h4>
                  <button
                    type="button"
                    className="close"
                    data-dismiss="modal"
                    aria-hidden="true"
                  >
                    ×
                  </button>
                </div>
                <div className="modal-body">
                  <div className="form-group">
                    <label>
                      Order Name <span className="text-danger">*</span>
                    </label>
                    <input
                      type="text"
                      name="editOrderName"
                      className="form-control"
                      value={this.state.orderName}
                      onChange={(e) =>
                        this.setState({ orderName: e.target.value })
                      }
                    />
                    {this.validator.message(
                      "editOrderName",
                      this.state.orderName,
                      "required|alpha_num_space|max:30",
                      { className: "text-danger" }
                    )}
                  </div>
                  <div className="form-group">
                    <label>
                      Order Price <span className="text-danger">*</span>
                    </label>
                    <input
                      type="text"
                      className="form-control"
                      name="editOrderPrice"
                      value={this.state.orderPrice}
                      onChange={(e) =>
                        this.setState({ orderPrice: e.target.value })
                      }
                    />
                    {this.validator.message(
                      "editOrderPrice",
                      this.state.orderPrice,
                      "required|numeric|min:1,num",
                      { className: "text-danger" }
                    )}
                  </div>
                  <div className="form-group">
                    <label>Order Notes</label>
                    <textarea
                      className="form-control"
                      value={this.state.orderNotes}
                      onChange={(e) =>
                        this.setState({ orderNotes: e.target.value })
                      }
                    />
                  </div>
                </div>
                <div className="modal-footer">
                  <input
                    type="button"
                    className="btn btn-default"
                    data-dismiss="modal"
                    defaultValue="Cancel"
                  />
                  <input type="submit" className="btn btn-info" value="Edit" />
                </div>
              </form>
            </div>
          </div>
        </div>
        {/* Edit Modal JSX */}

        <div id="deleteOrderModal" className="modal fade">
          <div className="modal-dialog">
            <div className="modal-content">
              <form method="post" onSubmit={this.handleDeleteConfirm}>
                <div className="modal-header">
                  <h4 className="modal-title">Delete Order</h4>
                  <button
                    type="button"
                    className="close"
                    data-dismiss="modal"
                    aria-hidden="true"
                  >
                    ×
                  </button>
                </div>
                <div className="modal-body">
                  <p>Are you sure you want to delete these records?</p>
                  <p className="text-warning">
                    <small>This action cannot be undone.</small>
                  </p>
                </div>
                <div className="modal-footer">
                  <input
                    type="button"
                    className="btn btn-default"
                    data-dismiss="modal"
                    defaultValue="Cancel"
                  />
                  <input
                    type="submit"
                    className="btn btn-danger"
                    value="Delete"
                  />
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default DashboardView;

import { all, fork, put, takeEvery } from "redux-saga/effects";
import * as dashboardActions from "../actions";
import { ADD_DASHBOARD_ORDER } from "../actions/types";

function* addDashboardOrder({ data }) {
  yield put(dashboardActions.enableLoader());
  yield put(dashboardActions.setDashboardOrder(data.objToAdd));
  yield put(dashboardActions.disableLoader());
}

export function* watchLoginUser() {
  yield takeEvery(ADD_DASHBOARD_ORDER, addDashboardOrder);
}

export default function* dashboardSaga() {
  yield all([fork(watchLoginUser)]);
}

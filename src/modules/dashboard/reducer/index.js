import {
  ENABLE_LOADER,
  DISABLE_LOADER,
  SET_DASHBOARD_ORDER,
  UPDATE_DASHBOARD_ORDER,
} from "../actions/types";
import * as storageUtils from "../../../utils/storageUtils";

const oldData = storageUtils.getDashboardOrderData();

const initialState = {
  isLoading: false,
  orders: oldData === null ? [] : oldData,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case DISABLE_LOADER:
      return {
        ...state,
        isLoading: false,
      };

    case ENABLE_LOADER:
      return {
        ...state,
        isLoading: true,
      };

    case SET_DASHBOARD_ORDER:
      const ordersData = [...state.orders];
      // new order on top of all
      ordersData.unshift(action.data);
      // store new array into localstorage
      storageUtils.setDashboardOrderData(ordersData);
      return {
        ...state,
        orders: ordersData,
      };

    case UPDATE_DASHBOARD_ORDER:
      // store new array into localstorage
      storageUtils.setDashboardOrderData(action.data);
      return {
        ...state,
        orders: action.data,
      };

    default:
      return state;
  }
};

import * as actions from "../actions/types";

export const enableLoader = () => ({
  type: actions.ENABLE_LOADER,
});

export const disableLoader = () => ({
  type: actions.DISABLE_LOADER,
});

export const addDashboardOrder = (data) => ({
  type: actions.ADD_DASHBOARD_ORDER,
  data,
});

export const setDashboardOrder = (data) => ({
  type: actions.SET_DASHBOARD_ORDER,
  data,
});

export const updateDashboardOrder = (data) => ({
  type: actions.UPDATE_DASHBOARD_ORDER,
  data,
});

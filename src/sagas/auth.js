import { all, fork, put, takeEvery } from "redux-saga/effects";

import { LOGIN_USER } from "../actions";

import { loginUserSuccess } from "../actions";

function* loginWithEmailPassword({ payload }) {
  try {
    let dummyText = { data: "dSVp8THENR2RKLYLX49XdELDhO2pBEMPRD0utbWe8ps" };
    localStorage.setItem("userData", JSON.stringify(dummyText));
    yield put(loginUserSuccess(dummyText));
  } catch (error) {
    console.log("login error : ", error);
  }
}

export function* watchLoginUser() {
  yield takeEvery(LOGIN_USER, loginWithEmailPassword);
}

export default function* rootSaga() {
  yield all([fork(watchLoginUser)]);
}

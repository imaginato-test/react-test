import { LOGIN_USER_SUCCESS } from "../actions";

const INIT_STATE = {
  user:
    JSON.parse(localStorage.getItem("userData")) !== null
      ? JSON.parse(localStorage.getItem("userData")).data
      : null,
  loading: false,
  type: "",
  isLoggedIn: false,
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case LOGIN_USER_SUCCESS:
      return {
        ...state,
        loading: false,
        user: action.payload.data,
        type: action.type,
        isLoggedIn: true,
      };
    default:
      return { ...state };
  }
};
